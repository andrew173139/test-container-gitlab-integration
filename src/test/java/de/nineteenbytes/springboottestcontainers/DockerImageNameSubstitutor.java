package de.nineteenbytes.springboottestcontainers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.testcontainers.utility.DockerImageName;
import org.testcontainers.utility.ImageNameSubstitutor;

@Slf4j
public class DockerImageNameSubstitutor extends ImageNameSubstitutor {

    public static final String DEFAULT_REGISTRY = "registry.proitr.ru/eiis-rih/pms/";

    @Override
    public DockerImageName apply(DockerImageName original) {
        log.debug("Applying image name substitution to {}", original.asCanonicalNameString());

        var registry = original.getRegistry();

        if (StringUtils.hasLength(registry)) {
            log.info("Image already has registry specified. Skipping substitution. registry={}", registry);
            return original;
        }
        return DockerImageName.parse(DEFAULT_REGISTRY + original.asCanonicalNameString());
    }

    @Override
    protected String getDescription() {
        return String.format("Add registry prefix: %s", DEFAULT_REGISTRY);
    }

}
